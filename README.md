# Brigapp 🚨:

Brigapp is an app that helps BEU (Brigada estudiantil uniandina) to manage emergencies. 

## Features:
- Report emergencies
- Login/register as a brigadista or as a generic user
- Manage emergencies, ask for backup and close emergencies
- Automatically turn on flashligt in low light conditions

## How to deploy it locally:

For deploying the app in Android:
- clone the repo
- open the repo directory in Android Studio
- ...

## Demo:
...