package com.uniandes.beuapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.uniandes.beuapp.R
import kotlinx.android.synthetic.main.fragment_main_button.*


class MainButtonFragment: Fragment(){


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_button, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val emergencyButton = reportButton;
        emergencyButton.setOnClickListener {
            (activity as MainActivity).showEmergencyInMap();
        }
    }

}
