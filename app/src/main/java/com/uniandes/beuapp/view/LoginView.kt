package com.uniandes.beuapp.view

/**
 * Class that represents LoginView
 * @author Miguel Angel Puentes
 */
interface LoginView {

    /**
     * Method that shows progress bar during a process.
     */
    fun showProgressBar()

    /**
     * Method that shows an information message when users logs out of the application.
     */
    fun showLogoutMessage()
}