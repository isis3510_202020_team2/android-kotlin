package com.uniandes.beuapp.view

/**Class that represents the SignUp view
 * @author María Camila González
 */
interface SignupView {

    /**
     * Method that shows progress bar during a process.
     */
    fun showProgressBar()

    /**
     * Method that shows an information message when users succesfuly register to the application.
     */
    fun showSuccessMessage()
}