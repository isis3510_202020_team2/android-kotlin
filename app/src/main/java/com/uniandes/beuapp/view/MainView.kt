package com.uniandes.beuapp.view

/**
 * Class that represents MainView
 * @author Miguel Angel Puentes
 */
interface MainView {
    /**
     * Method that shows the location in the map of the reported emergency.
     */
    fun showEmergencyInMap()
}