package com.uniandes.beuapp.view

import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.hardware.camera2.CameraAccessException
import android.hardware.camera2.CameraManager
import android.os.Build
import android.os.Bundle
import android.widget.ToggleButton
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.uniandes.beuapp.R
import com.uniandes.beuapp.presenter.TorchPresenter
import com.uniandes.beuapp.presenter.TorchPresenterImpl
import kotlinx.android.synthetic.main.activity_torch.*

/**
 * Class that represents TorchActivity
 * @author Miguel Angel Puentes
 */
class TorchActivity : AppCompatActivity(), TorchView, SensorEventListener {

    private var presenter: TorchPresenter? = null

    private lateinit var sensorManager: SensorManager
    private var light: Sensor? = null

    private lateinit var cameraManager: CameraManager
    private lateinit var cameraId: String


    /**
     * @see LoginView.showProgressBar
     */
    override fun showProgressBar(){

    }

    /**
     * @see AppCompatActivity.onCreate
     */
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_torch)

        // Presenter initialization
        presenter = TorchPresenterImpl(this)

        // Sensor Manager
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        light = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT)


        // Camera torch
        val isFlashAvailable = applicationContext.packageManager
            .hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT)
        if (!isFlashAvailable) {
            showNoFlashError()
        }
        cameraManager = getSystemService(Context.CAMERA_SERVICE) as CameraManager
        try {
            cameraId = cameraManager.cameraIdList[0]
        } catch (e: Exception) {
            e.printStackTrace()
        }
        //toggleButton = findViewById(R.id.onOffFlashlight)
        //toggleButton.setOnCheckedChangeListener { _, isChecked -> switchFlashLight(isChecked) }
    }

    override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {
        // Do something here if sensor accuracy changes.
    }

    override fun onSensorChanged(event: SensorEvent) {
        val lx = event.values[0]
        // Do something with this sensor data.
        var labelIllumination = illuminationText
        labelIllumination.text = lx.toString()

        if (lx < 10){
            switchFlashLight(true)
        } else {
            switchFlashLight(false)
        }

    }

    override fun onResume() {
        // Register a listener for the sensor.
        super.onResume()
        sensorManager.registerListener(this, light, SensorManager.SENSOR_DELAY_NORMAL)
    }

    override fun onPause() {
        // Be sure to unregister the sensor when the activity pauses.
        super.onPause()
        sensorManager.unregisterListener(this)
    }

    private fun showNoFlashError() {
        val alert = AlertDialog.Builder(this)
            .create()
        alert.setTitle("Oops!")
        alert.setMessage("Flash not available in this device...")
        alert.setButton(DialogInterface.BUTTON_POSITIVE, "OK") { _, _ -> finish() }
        alert.show()
    }
    private fun switchFlashLight(status: Boolean) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                cameraManager.setTorchMode(cameraId, status)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}