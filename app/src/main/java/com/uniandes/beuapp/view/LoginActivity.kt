package com.uniandes.beuapp.view

import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.uniandes.beuapp.R
import com.uniandes.beuapp.presenter.LoginPresenter
import com.uniandes.beuapp.presenter.LoginPresenterImpl

/**
 * Class that represents LoginActivity
 * @author Miguel Angel Puentes
 */
class LoginActivity : AppCompatActivity(), LoginView {

    private var presenter: LoginPresenter? = null

    /**
     * @see LoginView.showProgressBar
     */
    override fun showProgressBar(){

    }

    /**
     * @see LoginView.showLogoutMessage
     */
    override fun showLogoutMessage(){

    }

    /**
     * @see AppCompatActivity.onCreate
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Presenter initialization
        presenter = LoginPresenterImpl(this)

        //val navView: BottomNavigationView = findViewById(R.id.nav_view)

        //val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
       /* val appBarConfiguration = AppBarConfiguration(setOf(
            R.id.navigation_home,
            R.id.navigation_dashboard,
            R.id.navigation_notifications
        ))
        setupActionBarWithNavController(navController, appBarConfiguration)

        */
        //navView.setupWithNavController(navController)
    }
}