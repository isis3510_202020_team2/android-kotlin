package com.uniandes.beuapp.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import com.uniandes.beuapp.R
import com.uniandes.beuapp.presenter.LoginPresenter
import com.uniandes.beuapp.presenter.LoginPresenterImpl
import com.uniandes.beuapp.presenter.SignupPresenter
import com.uniandes.beuapp.presenter.SignupPresenterImpl

class SignupActivity : AppCompatActivity(), SignupView {

    private var presenter: SignupPresenter? = null

    /**
     * @see SignupView.showProgressBar
     */
    override fun showProgressBar(){

    }

    /**
     * @see SignupView.showLogoutMessage
     */
    override fun showSuccessMessage(){

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Presenter initialization
        presenter = SignupPresenterImpl(this)

        //val navView: BottomNavigationView = findViewById(R.id.nav_view)

        //val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(setOf(
            R.id.navigation_home,
            R.id.navigation_dashboard,
            R.id.navigation_notifications
        ))
        //setupActionBarWithNavController(navController, appBarConfiguration)
        //navView.setupWithNavController(navController)
    }
}