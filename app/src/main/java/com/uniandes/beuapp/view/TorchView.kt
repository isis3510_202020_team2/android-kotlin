package com.uniandes.beuapp.view


/**
 * Class that represents TorchView
 * @author Miguel Angel Puentes
 */
interface TorchView {

    /**
     * Method that shows progress bar during a process.
     */
    fun showProgressBar()
}