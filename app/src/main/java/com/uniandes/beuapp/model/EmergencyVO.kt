package com.uniandes.beuapp.model

/**
 * Class that represents EmergencyVO
 * @author Miguel Angel Puentes
 */
class EmergencyVO(
    override val longitude: String,
    override val altitude: String,
    override val additionalInfo: String,
    override val state: String
) : Emergency {

    private var counter: Int = 0

    override fun incrementCounter() {
        counter++
    }
}