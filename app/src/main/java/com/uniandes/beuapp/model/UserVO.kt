package com.uniandes.beuapp.model

/**
 * Class that represents UserVO
 * @author Miguel Angel Puentes
 */
class UserVO(
    override val name: String,
    override val lastName: String,
    override val email: String,
    override val age: String,
    override val phoneNumber: String
) : User {

    private var counter: Int = 0

    override fun incrementCounter() {
        counter++
    }
}