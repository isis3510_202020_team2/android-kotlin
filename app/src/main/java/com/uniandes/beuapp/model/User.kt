package com.uniandes.beuapp.model

/**
 * Class that represents User
 * @author Miguel Angel Puentes
 */
interface User {

    val name: String
    val lastName: String
    val email: String
    val age: String
    val phoneNumber: String

    fun incrementCounter()
}
