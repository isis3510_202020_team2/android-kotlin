package com.uniandes.beuapp.model

/**
 * Class that represents an Emergency
 * @author Miguel Angel Puentes
 */
interface Emergency {

    val longitude: String
    val altitude: String
    val additionalInfo: String
    val state: String

    fun incrementCounter()
}