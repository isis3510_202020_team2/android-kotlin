package com.uniandes.beuapp.presenter

import com.uniandes.beuapp.model.UserVO
import com.uniandes.beuapp.view.LoginView

/**
 * Class that represents LoginPresenterImp
 * @author Miguel Angel Puentes
 */
class LoginPresenterImpl(private val loginView: LoginView): LoginPresenter {


    /**
     * User's reference
     */
    private lateinit var user: UserVO

    /**
     * @see LoginPresenter.logIn
     */
    override fun logIn(email:String, password: String): Boolean? {
        //Login Logic code
        var answ = false
        if (email.equals("example.com") && password.equals("0000")){
            var user = UserVO("Juan","Perez","example.com","20","x");
            var answ = true;
            loginView.showProgressBar();

        }
        return answ;
    }

    /**
     * @see LoginPresenter.logOut
     */
    override fun logOut(): Boolean? {
       loginView.showLogoutMessage()
       return true;
    }

}