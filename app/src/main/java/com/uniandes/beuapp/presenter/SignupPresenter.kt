package com.uniandes.beuapp.presenter

interface SignupPresenter {


    /**
     * Method that registers the user's account.
     */
    fun signUp(email: String, password: String, name: String, lastName: String, phoneNumber: String, age: String): Boolean?


}