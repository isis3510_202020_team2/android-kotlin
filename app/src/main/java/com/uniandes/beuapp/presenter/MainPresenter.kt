package com.uniandes.beuapp.presenter

/**
 * Class that represents MainPresenter
 * @author Miguel Angel Puentes
 */
interface MainPresenter {

    /**
     * Method that reports an emergency in the current location.
     */
    fun reportEmergency(x: String, y: String, additionalInfo: String): Boolean?

    /**
     * Method that deletes emergency selected.
     */
    fun deleteEmergency() : Boolean?
}