package com.uniandes.beuapp.presenter

import com.uniandes.beuapp.model.UserVO
import com.uniandes.beuapp.view.LoginView
import com.uniandes.beuapp.view.TorchView

/**
 * Class that represents TorchPresenterImp
 * @author Miguel Angel Puentes
 */
class TorchPresenterImpl(private val torchView: TorchView): TorchPresenter {


    /**
     * User's reference
     */
    private lateinit var user: UserVO



}