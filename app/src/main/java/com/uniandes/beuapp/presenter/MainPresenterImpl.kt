package com.uniandes.beuapp.presenter

import android.location.Location
import android.location.LocationListener
import com.uniandes.beuapp.model.EmergencyVO
import com.uniandes.beuapp.model.UserVO
import com.uniandes.beuapp.view.LoginView
import com.uniandes.beuapp.view.MainView

/**
 * Class that represents MainPresenterImpl
 * @author Miguel Angel Puentes
 */
class MainPresenterImpl(private val mainView: MainView): MainPresenter, LocationListener {

    /**
     * Emergency's reference
     */
    private lateinit var currentEmergency: EmergencyVO

    override fun reportEmergency(x: String, y: String, additionalInfo: String): Boolean? {
        TODO("Not yet implemented")
    }

    override fun deleteEmergency(): Boolean? {
        TODO("Not yet implemented")
    }

    override fun onLocationChanged(p0: Location) {


        mainView.showEmergencyInMap();
        TODO("Not yet implemented")
    }


}