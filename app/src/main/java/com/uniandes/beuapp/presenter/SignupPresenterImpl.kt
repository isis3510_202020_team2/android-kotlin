package com.uniandes.beuapp.presenter

import com.uniandes.beuapp.model.UserVO
import com.uniandes.beuapp.view.LoginView
import com.uniandes.beuapp.view.SignupView

class SignupPresenterImpl(private val signupView: SignupView): SignupPresenter
{

    /**
     * @see SignupPresenter.Signup
     */
    override fun signUp(email: String, password: String, name: String, lastName: String, phoneNumber: String, age: String): Boolean? {
        //Login Logic code
        var answ = registerInDB(email, password, name, lastName, phoneNumber, age);
        if (answ!!){

            signupView.showProgressBar();
            signupView.showSuccessMessage();

        }
        return answ;
    }

    private fun registerInDB(email: String, password: String, name: String, lastName: String, phoneNumber: String, age: String): Boolean? {
        //TODO Escribir en la BD
        return true;
    }

}