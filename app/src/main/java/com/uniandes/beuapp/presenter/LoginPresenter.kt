package com.uniandes.beuapp.presenter

import com.uniandes.beuapp.view.LoginView

/**
 * Class that represents LoginPresenter
 * @author Miguel Angel Puentes
 */
interface LoginPresenter {

    /**
     * Method that starts the session of the user.
     */
    fun logIn(email: String, password: String): Boolean?

    /**
     * Method that ends the session of the user.
     */
    fun logOut() : Boolean?
}